// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefense_GAMPRG2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TowerDefense_GAMPRG2, "TowerDefense_GAMPRG2" );

DEFINE_LOG_CATEGORY(LogTowerDefense_GAMPRG2)
 