// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Spawner.h"
#include "SpawnInfo.h"
#include "HealthComponent.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::StartNewWave()
{
	GetWorldTimerManager().SetTimer(waveSpawnHandle, this, &ASpawner::SpawnEnemies, waveDelay);
}

void ASpawner::SpawnEnemies()
{	
	if (currentWave >= spawnInfoArray.Num())
	{
		return;
	}

	FVector location = GetActorLocation();
	FRotator rotation(0.0f, 0.0f, 0.0f);

	AEnemy* enemy = GetWorld()->SpawnActor<AEnemy>(spawnInfoArray[currentWave]->enemiesToSpawn[enemyIndex], location, rotation);
	enemy->SetTargetWaypoint(targetWaypoint);
	
	enemyIndex++;

	if (enemyIndex <= spawnInfoArray[currentWave]->enemiesToSpawn.Num() - 1)
	{
		GetWorldTimerManager().SetTimer(enemySpawnHandle, this, &ASpawner::SpawnEnemies, spawnDelay);
	}

	enemy->healthComponent->health *= ((healthMultiplier * currentWave) + 1);
	enemy->healthComponent->OnDie.AddDynamic(this, &ASpawner::OnEnemyDeath);

	currentEnemies++;

	OnEnemySpawn.Broadcast(1.0f);
}

void ASpawner::ResetValues()
{
	enemyIndex = 0;
}

void ASpawner::OnEnemyDeath()
{
	currentEnemies--;
	OnEnemySpawn.Broadcast(-1.0f);

	if (currentEnemies == 0)
	{
		currentWave++;
		ResetValues();
		OnWaveClear.Broadcast();
	}
}
