// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

class AProjectile;

UCLASS()
class TOWERDEFENSE_GAMPRG2_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float fireRate;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float projDamage;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* TowerBase;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* TowerHead;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereCollider;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<AProjectile> projectile;

	UFUNCTION()
		void Shoot(FRotator rotation);

	UFUNCTION()
		void FindEnemies();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<AActor*> targets;

	FTimerHandle detectEnemiesHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
