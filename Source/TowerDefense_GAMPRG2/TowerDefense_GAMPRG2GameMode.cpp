// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefense_GAMPRG2GameMode.h"
#include "TowerDefense_GAMPRG2PlayerController.h"
#include "TowerDefense_GAMPRG2Character.h"
#include "UObject/ConstructorHelpers.h"

ATowerDefense_GAMPRG2GameMode::ATowerDefense_GAMPRG2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATowerDefense_GAMPRG2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}