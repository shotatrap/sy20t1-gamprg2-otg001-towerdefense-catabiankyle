// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TowerBase = CreateDefaultSubobject<UStaticMeshComponent>("Tower Base");

	TowerHead = CreateDefaultSubobject<UStaticMeshComponent>("Tower Head");
	TowerHead->SetupAttachment(TowerBase);

	Arrow = CreateDefaultSubobject<UArrowComponent>("Arrow");
	Arrow->SetupAttachment(TowerHead);

	SphereCollider = CreateDefaultSubobject<USphereComponent>("Sphere Collider");
	SphereCollider->SetupAttachment(TowerBase);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(detectEnemiesHandle, this, &ATower::FindEnemies, fireRate, true);
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATower::Shoot(FRotator rotation)
{
	AProjectile* projToFire = GetWorld()->SpawnActor<AProjectile>(projectile, Arrow->GetComponentLocation(), rotation);
	projToFire->damage = projDamage;
}

void ATower::FindEnemies()
{
	SphereCollider->GetOverlappingActors(targets, AEnemy::StaticClass());

	if (targets.Num() > 0)
	{
		FRotator heightRotation = UKismetMathLibrary::FindLookAtRotation(Arrow->GetComponentLocation(), targets[0]->GetActorLocation());
		Shoot(heightRotation);
		FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), targets[0]->GetActorLocation());
		rotation.Yaw -= 90.0f;
		TowerHead->SetWorldRotation(rotation);
	}
}

