// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

class UHealthComponent;

UCLASS()
class TOWERDEFENSE_GAMPRG2_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 movespeed;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool isGround;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool isNormal;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 baseDamage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UHealthComponent* healthComponent;

	UFUNCTION()
		void Die();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void MoveToWaypoints();

	UFUNCTION()
	void SetTargetWaypoint(AActor* passedWaypoint);

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	AActor* TargetWaypoint;
};
