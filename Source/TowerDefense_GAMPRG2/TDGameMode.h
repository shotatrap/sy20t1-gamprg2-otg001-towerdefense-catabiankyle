// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_GAMPRG2_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

private:
    int32 finishedSpawners;

    TArray<AActor*> spawners;

public:
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
        int32 currentWave = 1;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
        float enemyCount;

    UFUNCTION()
        void SpawnNextWave();

    UFUNCTION()
        void BindSpawners();

    UFUNCTION()
        void IncrementCount(float count);

protected:
    virtual void BeginPlay() override;

};
