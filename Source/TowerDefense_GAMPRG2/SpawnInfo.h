// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpawnInfo.generated.h"

/**
 * 
 */

class AEnemy;

UCLASS()
class TOWERDEFENSE_GAMPRG2_API USpawnInfo : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<TSubclassOf<AEnemy>> enemiesToSpawn;
};
