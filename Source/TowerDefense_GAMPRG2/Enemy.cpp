// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAIController.h"
#include "Enemy.h"
#include "Waypoint.h"
#include "HealthComponent.h"
#include <Runtime\Engine\Classes\Kismet\GameplayStatics.h>

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	healthComponent = CreateDefaultSubobject<UHealthComponent>("Health Component");
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	healthComponent->OnDie.AddDynamic(this, &AEnemy::Die);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::MoveToWaypoints()
{
	AEnemyAIController* EnemyController = Cast<AEnemyAIController>(GetController());

	if (EnemyController)
	{
		AWaypoint* Waypoint = Cast<AWaypoint>(TargetWaypoint);

		if (Waypoint)
		{
			EnemyController->MoveToActor(Waypoint, -1.0f, false);

			if (Waypoint->GetNextWaypoint() != nullptr)
			{
				TargetWaypoint = Waypoint->GetNextWaypoint();
			}
			else
			{
				TargetWaypoint = NULL;
			}
		}
	}
}

void AEnemy::SetTargetWaypoint(AActor* passedWaypoint)
{
	TargetWaypoint = passedWaypoint;

	MoveToWaypoints();
}

void AEnemy::Die()
{
	Destroy();
}
