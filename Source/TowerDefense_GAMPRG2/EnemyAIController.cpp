// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "EnemyAIController.h"

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	AEnemy* Enemy = Cast<AEnemy>(GetPawn());

	if (Enemy)
	{
		Enemy->MoveToWaypoints();
	}
}

