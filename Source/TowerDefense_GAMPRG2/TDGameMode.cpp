// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"
#include "Kismet/GameplayStatics.h"
#include "TDGameMode.h"

void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), spawners);

	BindSpawners();
}

void ATDGameMode::SpawnNextWave()
{
	finishedSpawners++;

	if (finishedSpawners == spawners.Num())
	{
		finishedSpawners = 0;

		currentWave++;

		for (AActor* Actor : spawners)
		{
			ASpawner* Spawner = Cast<ASpawner>(Actor);

			if (Spawner)
			{
				Spawner->StartNewWave();
			}
		}
	}
}

void ATDGameMode::BindSpawners()
{
	for (AActor* Actor : spawners)
	{
		ASpawner* Spawner = Cast<ASpawner>(Actor);

		if (Spawner)
		{
			Spawner->OnWaveClear.AddDynamic(this, &ATDGameMode::SpawnNextWave);
			Spawner->OnEnemySpawn.AddDynamic(this, &ATDGameMode::IncrementCount);
			Spawner->StartNewWave();
		}
	}
}

void ATDGameMode::IncrementCount(float count)
{
	enemyCount += count;
}
