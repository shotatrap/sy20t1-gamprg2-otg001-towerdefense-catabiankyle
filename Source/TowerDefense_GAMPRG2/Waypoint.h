// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Waypoint.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_GAMPRG2_API AWaypoint : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	UFUNCTION()
	AActor* GetNextWaypoint();
	
private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	AActor* nextWaypoint;
};
