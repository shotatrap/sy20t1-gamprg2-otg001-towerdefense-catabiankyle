// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefense_GAMPRG2GameMode.generated.h"

UCLASS(minimalapi)
class ATowerDefense_GAMPRG2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATowerDefense_GAMPRG2GameMode();
};



