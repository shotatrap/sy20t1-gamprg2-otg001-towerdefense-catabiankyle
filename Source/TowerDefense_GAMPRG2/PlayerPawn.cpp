// Fill out your copyright notice in the Description page of Project Settings.


#include "Base.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawneds
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABase::StaticClass(), cores);

	BindCores();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayerPawn::BindCores()
{
	for (AActor* Actor : cores)
	{
		ABase* Base = Cast<ABase>(Actor);

		if (Base)
		{
			Base->OnEnemyHit.AddDynamic(this, &APlayerPawn::TakeDamage);
		}
	}
}

void APlayerPawn::TakeDamage(int32 damage)
{
	health -= damage;
}

