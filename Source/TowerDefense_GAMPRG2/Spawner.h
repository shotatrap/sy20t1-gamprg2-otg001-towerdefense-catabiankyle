// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class USpawnInfo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWaveClear);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEnemiesSpawned, float, count);

UCLASS()
class TOWERDEFENSE_GAMPRG2_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(BlueprintAssignable)
	FWaveClear OnWaveClear;

	UPROPERTY(BlueprintAssignable)
	FEnemiesSpawned OnEnemySpawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<USpawnInfo*> spawnInfoArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 currentWave;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float healthMultiplier;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AActor* targetWaypoint;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 currentEnemies;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float spawnDelay;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float waveDelay;

	FTimerHandle enemySpawnHandle;

	FTimerHandle waveSpawnHandle;

	UFUNCTION()
		void SpawnEnemies();

	UFUNCTION()
		void StartNewWave();

	UFUNCTION()
		void OnEnemyDeath();

	UFUNCTION()
		void ResetValues();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 enemyIndex;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
